use glob::*;

pub fn jpg_glob() -> Result<Paths, PatternError> {
    glob("*.jpg")
}
