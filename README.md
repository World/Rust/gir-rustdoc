# gir-rustdoc

Tooling for docs of rust gir based projects.

Have a look at [`.gitlab-ci.yml`](.gitlab-ci.yml) for how to use.

- https://world.pages.gitlab.gnome.org/Rust/gir-rustdoc/

```sh
usage: gir-rustdoc.py [-h] [--pages-url PAGES_URL] [--branch BRANCH]
                      [--default-branch DEFAULT_BRANCH]
                      [--project-url PROJECT_URL]
                      [--project-title PROJECT_TITLE] [--releases RELEASES]
                      [--job-token JOB_TOKEN]
                      {pre-docs,html-index,docs-from-artifacts} ...

Helps to generate docs pages for Rust gir based projects.

positional arguments:
  {pre-docs,html-index,docs-from-artifacts}
    pre-docs            Injects JavaScript code via RUSTDOCFLAGS for outdated
                        version warnings.
    html-index          Creates public/index.html with overview of all versions.
                        Also creates LATEST_RELEASE_BRANCH required by
                        JavaScript version checker.
    docs-from-artifacts
                        GitLab CI only. Pull artifact for each version and embed
                        to public/. Does compression as well
```
## Creating releases on GitLab

- Create release branch
- Adjust dependencies to correct (fixed) version on the release branch
- Add `<branch name>=<version name>` to the *beginning/top* of the `RELEASES` variable on the default branch

Note: After changes to the release branch you have to manually rebuild the default branch for the changes to take effect.

## Projects using gir-rustdoc

#### GitHub

- [gtk-rs-core](https://gtk-rs.org/gtk-rs-core/)
- [gtk3-rs](https://gtk-rs.org/gtk3-rs)
- [gtk4-rs](https://gtk-rs.org/gtk4-rs)

### GitLab

- [gstreamer-rs](https://gstreamer.pages.freedesktop.org/gstreamer-rs/)
- [libhandy-rs](https://world.pages.gitlab.gnome.org/Rust/libhandy-rs/)
- [libadwaita-rs](https://world.pages.gitlab.gnome.org/Rust/libadwaita-rs/)
- [sourceview5-rs](https://world.pages.gitlab.gnome.org/Rust/sourceview5-rs/)
